package exmaple.groovylang

import example.spock.objectbuilder.Cargo
import example.spock.objectbuilder.CargoOrder

/**
 * Groovy는 생성자를 만들 때
 */
class MapBasedConstructor {
    static void main(String[] args)     {
        Map<String, Integer> scores = ["Math":60, "English":70, "Korean": 90]
        println "Math:$scores.Math, English:$scores.English, Korean:$scores.Korean"
        Cargo cargo = new Cargo(type:"cargo1", cargoOrder: new CargoOrder(buyer: "Dennis", city: "Seoul", price: 1000), tons: 1.2)
        println "$cargo.type, $cargo.cargoOrder.buyer, $cargo.cargoOrder.city, $cargo.tons"
    }
}

