package exmaple.groovylang

class ClosureExample {
    static void main(String[] args)     {
        Closure simple = { int x -> return x * 2}
        println simple(3)
    }
}
