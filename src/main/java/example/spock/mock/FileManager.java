package example.spock.mock;

public class FileManager {
    public int removeFiles(String directory)    {
        int count = 0;
        if(isDirectory(directory))  {
            String[] files = findFiles(directory);
            for(String file : files)    {
                deleteFile(file);
                count++;
            }
        }
        return count;
    }

    public boolean isDirectory(String directory) {
        return directory.endsWith("/");
    }

    public String[] findFiles(String directory) {
        // read files from disk.
        return null;
    }

    public void deleteFile(String file)    {
        // delete a file.
        return;
    }
}
