package example.spock.mock;

class FooService {
    private boolean executed = false;

    String doSomething(String name) {
        String msg = "Hi " + name + ", FooService did something";
        System.out.println(msg);
        return msg;
    }

    public void executed()  {
        executed = true;
    }

    public boolean isExecuted()    {
        return executed;
    }
}