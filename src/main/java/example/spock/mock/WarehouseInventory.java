package example.spock.mock;

import java.util.HashMap;
import java.util.Map;

public class WarehouseInventory {
    private Map<String, Integer> inventoryMap = new HashMap<>();

    public Integer getProductInventoryCount(String productName)   {
        Integer inventoryCount = inventoryMap.get(productName);
        if(inventoryCount == null) throw new IllegalArgumentException(String.format("%s does not exist", productName));
        else return inventoryCount;
    }

    public void setProductInventoryCount(String productName, int stocks) {
        inventoryMap.put(productName, stocks < 0 ? Integer.valueOf(0) : Integer.valueOf(stocks));
    }

    public void setProductInventories(String[] products, Integer[] stocks)  {
        for(int i = 0;i < products.length;i++)   {
            setProductInventoryCount(products[i], stocks[i]);
        }
    }

    public boolean isEmpty()    {
        return inventoryMap.size() == 0;
    }
}
