package example.spock.mock;

import java.util.ArrayList;
import java.util.List;

public class Basket {
    private List<Product> addedProducts = new ArrayList<>();
    private WarehouseInventory warehouseInventory;

    public Basket(WarehouseInventory inventory) {
        this.warehouseInventory = inventory;
    }

    public void addProduct(Product product)     {
        addedProducts.add(product);
    }

    public boolean canShipCompletely()  {
        if(warehouseInventory.isEmpty()) return false;

        for(Product product : addedProducts)    {
            Integer inventoryCount = warehouseInventory.getProductInventoryCount(product.getName());
            if(inventoryCount == null || inventoryCount == 0)   {
                return false;
            }
        }

        return true;
    }

    public String toString()    {
        StringBuilder stringBuilder = new StringBuilder();
        for(Product p : addedProducts)  {
            stringBuilder.append(p.getName() + ":" + warehouseInventory.getProductInventoryCount(p.getName()) + ",");
        }
        return stringBuilder.toString();
    }
}
