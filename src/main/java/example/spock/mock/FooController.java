package example.spock.mock;

class FooController {
    FooService fooService;

    public String doSomething() {
        String msg =  fooService.doSomething("Sally");
        fooService.executed();
        System.out.println(msg);

        return msg;
    }
}