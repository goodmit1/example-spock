package example.spock.basic;

public class HeavyClass {
    public HeavyClass() {
        System.out.println("HeavyClass constructor is called");
    }

    public HeavyClass(String param) {
        System.out.println("HeavyClass constructor is called with parameter : " + param);
    }
}
