package example.spock.inheritance;

public class ChildClass extends ParentClass {
    private String childField1;

    public String getChildField1() {
        return childField1;
    }
}
