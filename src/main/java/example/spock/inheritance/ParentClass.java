package example.spock.inheritance;

public class ParentClass {
    private String parentField1;

    public String getParentField1() {
        return parentField1;
    }

    public void setParentField1(String parentField1) {
        this.parentField1 = parentField1;
    }
}
