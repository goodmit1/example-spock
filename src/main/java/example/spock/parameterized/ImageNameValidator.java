package example.spock.parameterized;

public class ImageNameValidator {
    public boolean isValidImageExtension(String fileName)   {
        return fileName.contains(".jpg") || fileName.contains(".png");
    }
}
