package example.spock.parameterized;

public class ModCalculator {
    public int mod(int value, int mod)   {
        return value % mod;
    }
}
