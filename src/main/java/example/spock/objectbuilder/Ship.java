package example.spock.objectbuilder;

import java.util.ArrayList;
import java.util.List;

public class Ship {
    private String name;
    private List<CrewMember> crewMembers = new ArrayList<>();
    private String destination;
    private List<Cargo> cargos= new ArrayList<>();
}
