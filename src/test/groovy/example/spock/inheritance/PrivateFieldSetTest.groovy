package example.spock.inheritance

import spock.lang.Specification

class PrivateFieldSetTest extends Specification{
    // 참고로, ParentClass의 field에 setter 메쏘드가 없으면 Spock에서는 값을 설정할 수 없다.
    def "상속된 부모 클래스의 private field에 값을 설정한다."() {
        when:
        ChildClass child = new ChildClass(childField1: "childfield1", parentField1: "parentfield2")
        then:
        child.getChildField1() == "childfield1"
        child.getParentField1() == "parentfield2"
    }
}
