package example.spock.timeout

import spock.lang.Specification
import spock.lang.Timeout

class TimeoutTest extends Specification {
    @Timeout(5)
    def "the method should be executed in 5 secs"() {
        given: "create a class under test"
        UnderTestClass sut = new UnderTestClass()
        when: "execute a method"
        sut.method1()
        then:
        sut.executed

    }
}

class UnderTestClass    {
    boolean executed = false
    void method1()   {
        try { Thread.sleep(4000); } catch(Exception e) {}
        executed = true
    }
}