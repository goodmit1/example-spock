package example.spock.expando

import spock.lang.Specification

class ExpandoExampleTest extends Specification  {
    def "You can create dummy object by Expando which is like a mock object"()  {
        when: "two different addresses are check"
        Address invalidAddr = new Address(country:"Korea", number:23)
        Address validAddr = new Address(country:"US", number:30, state:"NY", zipcode: "123456")

        def addressDao = new Expando()
        addressDao.load = { id -> return id == 1 ? invalidAddr : validAddr }
        Stamper stamper = new Stamper(addressDao as AddressDao)

        then: "Only the address with state and zipcode is accepted"
        !stamper.isValid(1)
        stamper.isValid(2)
    }
}

class Address   {
    private String country;
    private int number;
    private String state;
    private String zipcode;
}

class Stamper   {
    private final AddressDao addressDao

    public Stamper(AddressDao addressDao)   {
        this.addressDao = addressDao
    }
    public boolean isValid(Long addressId) {
        Address address = addressDao.load(addressId)
        return address.state != null && address.zipcode != null
    }
}

interface AddressDao    {
    Address load(Long id)
}