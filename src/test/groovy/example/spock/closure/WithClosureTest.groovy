package example.spock.closure

import spock.lang.Specification

/**
 * Groovy의 Closure를 이용해서 List의 값에 대한 검증을 수행한다.
 * products.any { 함수 } : products list에 있는 아이템들을 파라미터에 넘어온 함수를 호출하여 결과값이 참이 나올 때까지 수행하고 true를 리턴한다.
 * 그렇지 않으면 false 리턴.
 * products.every { 함수 } : products list의 모든 아이템을 파라미터로 넘어온 함수를 호출하여 모두 true가 나오면 결과값이 true가 된다.
 * Closure에 대해서는 https://en.wikipedia.org/wiki/Closure_(computer_programming) 참조.
 */
class WithClosureTest extends Specification  {
    def "trivial test with Groovy closure"()    {
        given:
        List<String> products = ['camera', 'laptop', 'tv']
        expect:
        // products list중에 하나라도 camera가 있는가
        products.any { product -> product == "camera" }
        and:
        // products list 전체 아이템들은 hotdog 이름을 가지지 않는다.
        products.every { product -> product != "hotdog" }

    }
}
