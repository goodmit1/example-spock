package example.spock.hamcrest

import spock.lang.Specification

import static org.hamcrest.CoreMatchers.not
import static org.hamcrest.core.IsCollectionContaining.hasItem

class HamcrestTest extends Specification   {
    def "trivial test with Hamcrest"()  {
        given:
            List<String> products = ['camera', 'laptop', 'tv']
        expect:
            products hasItem("camera")
            // or you can use like the below.
//            expect(products, hasItem("camera"))
        and:
            products not(hasItem("radio"))
        // or you can use like the below.
//            that(products, not(hasItem("radio")))
    }
}
