package example.spock.basic

import spock.lang.Specification

/**
 * 각각의 test case가 수행되기 전과 후에 setup 및 cleanup이 필요한 작업이 있다면 setup(), cleanup() 이란 메소드를 작성하면 된다.
 */
class SetupCleanupTest extends Specification {
    Product tv
    Product camera

    def setup() {
        System.out.println("setup called")
        tv = new Product(name:"50In Samsung TV", price:400, weight:50)
        camera = new Product(name:"Canon 5S", price:800, weight:3)
    }

    def cleanup()   {
        System.out.println("cleanup called")
    }

    def "test1"()   {
        System.out.println("test1 is evaludated")
        expect:
        tv.price == 400
        camera.price == 800
    }

    def "test2"()   {
        System.out.println("test2 is evaludated")
        expect:
        tv.name == "50In Samsung TV"
        camera.name == "Canon 5S"
    }
}
