package example.spock.basic

import spock.lang.Specification

/**
 * 하나의 test case에 대한 cleanup 작업을 수행한다.
 */
class CleanupForMethodTest extends Specification {
    def "test case 수행 후 cleanup"()    {
        given:
            String hello = new Hello().hello()
        expect:
            hello == "Hello"
        cleanup:
            System.out.println("cleanup is called")
    }
    def "test case 수행 후 cleanup 없음"()    {
        given:
        String hello = new Hello().hello()
        expect:
        hello == "Hello"
    }
}
