package example.spock.basic

import spock.lang.Specification

/**
 * setup(), cleanup()은 하나의 Spec 내에 있는 모든 test case에 대해서 각각 호출되었다면 setupSpec(), cleanupSpec()은
 * 모든 test case에 대해서 한번만 호출된다.
 */
class SetupSpecCleanupSpecTest extends Specification {
    Product tv
    Product camera

    def setupSpec() {
        System.out.println("setupSpec is called")
    }

    def cleanupSpec()   {
        System.out.println("cleanupSpec is called")
    }

    def setup() {
        System.out.println("setup called")
        tv = new Product(name:"50In Samsung TV", price:400, weight:50)
        camera = new Product(name:"Canon 5S", price:800, weight:3)
    }

    def cleanup()   {
        System.out.println("cleanup called")
    }

    def "test1"()   {
        System.out.println("test1 is evaludated")
        expect:
        tv.price == 400
        camera.price == 800
    }

    def "test2"()   {
        System.out.println("test2 is evaludated")
        expect:
        tv.name == "50In Samsung TV"
        camera.name == "Canon 5S"
    }

}
