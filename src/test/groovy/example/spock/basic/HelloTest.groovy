package example.spock.basic
import spock.lang.Specification;

/**
 * given은 테스트가 수행되기 전에 사전작업이다. 테스트 검증 이전의 사전 작업이 있으면 여기에 넣어준다.
 * expect 영역에서 결과에 대한 검증 코드를 넣는다.
 */
class HelloTest extends Specification {
    def "Hello 객체는 hello 메세지를 주어야 한다"()    {
        given:
        String hello = new Hello().hello()
        expect:
        hello == "Hello"
    }
}