package example.spock.basic

import spock.lang.Shared
import spock.lang.Specification

/**
 * If you want to create an object only once for all test cases
 * You mark 'Shared' annotation for the object.
 *
 */
class SharedTest extends Specification  {
    @Shared
    HeavyClass heavyClass = new HeavyClass()
    HeavyClass heavyClass1 = new HeavyClass("SharedTest")

    def "test1"()   {
        expect: 1 == 1
    }

    def "test2"()   {
        expect: 1 == 1
    }
}
