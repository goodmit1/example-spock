package example.spock.basic

import spock.lang.Specification

/**
 * old() 메소드는 객체의 변경 전 값을 알 수 있는 기능을 제공한다.
 *
 */
class OldMethodTest extends  Specification  {
    def "상품의 이전 값은 현재값의 절반이어야 한다."() {
        given:
            Product p = new Product(name:"tv", price:100, weight:50)
        when:
            p.price = p.price * 2
        then:
            p.price == 200
            old(p.price) == p.price / 2
    }
}
