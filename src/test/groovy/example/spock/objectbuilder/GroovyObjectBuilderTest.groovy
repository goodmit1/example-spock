package example.spock.objectbuilder

import spock.lang.Specification
/**
 * 테스트 할 때 테스트를 위해 사전에 준비해야 하는 데이타가 복잡한 경우 이 데이타를 생성하는 작업이 매우 번거럽고 코드 작성에 시간이 많이 걸릴 수 있다.
 * Groovy에서 제공하는 ObjectGraphBuilder를 이용해서 빠르게 모의 데이타를 생성할 수 있다.
 *
 */
class GroovyObjectBuilderTest extends Specification {
    def "Data generation with ObjectGraphBuilder"()    {

        ObjectGraphBuilder builder = new ObjectGraphBuilder()
        builder.classNameResolver = "example.spock.objectbuilder"       // 생성할 객체가 있는 패키지 위치
        AssetInventory assetInventory = builder.assetInventory()    {   // builder.<class name>()
            ship(name:"Fox", destination:"Seoul") {
                crewMember(firstName:"Kevin", lastName:"Kim", age:40)
                crewMember(firstName:"Tony", lastName:"Kim", age:45)
                crewMember(firstName:"Dennis", lastName:"Kim", age:50)
                cargo(type:"Cotton", tons:5.4) {
                    cargoOrder ( buyer: "Rei Hosokawa",city:"Yokohama",price:34000)
                }
                cargo(type:"Olive Oil", tons:3.0) {
                    cargoOrder(buyer: "Hirokumi Kasaya", city: "Kobe", price: 27000)
                }
            }
            ship(name:"Wolf", destination:"Toko") {
                crewMember(firstName:"Kevin11", lastName:"Kim", age:40)
                crewMember(firstName:"Tony11", lastName:"Kim", age:45)
                crewMember(firstName:"Dennis11", lastName:"Kim", age:50)
                cargo(type:"Cotton11", tons:5.4) {
                    cargoOrder ( buyer: "Rei Hosokawa",city:"Yokohama",price:34000)
                }
                cargo(type:"Olive Oil11", tons:3.0) {
                    cargoOrder(buyer: "Hirokumi Kasaya", city: "Kobe", price: 27000)
                }
            }
        }

        expect:
        assert assetInventory.ships.size == 2
        assert assetInventory.ships[0].name == "Fox"
        assert assetInventory.ships[1].crewMembers.size == 3
        assert assetInventory.ships[1].crewMembers[0].firstName == "Kevin11"
    }
}