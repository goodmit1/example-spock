package example.spock.helper

import example.spock.mock.Basket
import example.spock.mock.Product
import example.spock.mock.WarehouseInventory
import example.spock.objectbuilder.AssetInventory
import spock.lang.Specification

/**
 * If a step is complicated and has lots of codes you can split them into a method.
 * For example, 'GroovyObjectBuilderTest' test has lots of codes for a test in a given and then steps.
 * It can be restructured like this example.
 */
class WithHelperTest extends Specification {
    def "Data generation with ObjectGraphBuilder"()    {
        given: "assert inventory contains ships"
        AssetInventory assetInventory = createAssetIventory()

        expect:
        assertThatInventoryHasCorrectShips(assetInventory)
    }

    def "inventory should be checked"()  {
        given:
        Product tv = new Product(name: "tv", price: 1000, weight: 1000)
        and:
        WarehouseInventory inventory = Mock(WarehouseInventory)
        Basket basket = new Basket(inventory)
        basket.addProduct(tv)

        when:
        basket.canShipCompletely()

        then:
        interaction {
            inventoryShouldBeChecked(inventory)
        }
    }

    private void inventoryShouldBeChecked(WarehouseInventory inventory) {
        1 * inventory.getProductInventoryCount(_)
        1 * inventory.isEmpty()
    }

    private AssetInventory createAssetIventory()  {
        ObjectGraphBuilder builder = new ObjectGraphBuilder()
        builder.classNameResolver = "example.spock.objectbuilder"

        return builder.assetInventory()    {
            ship(name:"Fox", destination:"Seoul") {
                crewMember(firstName:"Kevin", lastName:"Kim", age:40)
                crewMember(firstName:"Tony", lastName:"Kim", age:45)
                crewMember(firstName:"Dennis", lastName:"Kim", age:50)
                cargo(type:"Cotton", tons:5.4) {
                    cargoOrder ( buyer: "Rei Hosokawa",city:"Yokohama",price:34000)
                }
                cargo(type:"Olive Oil", tons:3.0) {
                    cargoOrder(buyer: "Hirokumi Kasaya", city: "Kobe", price: 27000)
                }
            }
            ship(name:"Wolf", destination:"Toko") {
                crewMember(firstName:"Kevin11", lastName:"Kim", age:40)
                crewMember(firstName:"Tony11", lastName:"Kim", age:45)
                crewMember(firstName:"Dennis11", lastName:"Kim", age:50)
                cargo(type:"Cotton11", tons:5.4) {
                    cargoOrder ( buyer: "Rei Hosokawa",city:"Yokohama",price:34000)
                }
                cargo(type:"Olive Oil11", tons:3.0) {
                    cargoOrder(buyer: "Hirokumi Kasaya", city: "Kobe", price: 27000)
                }
            }
        }
    }

    private void assertThatInventoryHasCorrectShips(AssetInventory assetInventory)  {
        with(assetInventory)    {
            assetInventory.ships.size() == 2
            assetInventory.ships[0].name == "Fox"
            assetInventory.ships[1].crewMembers.size() == 3
            assetInventory.ships[1].crewMembers[0].firstName == "Kevin11"
        }
    }
}
