package example.spock.parameterized

import spock.lang.Specification

class ModCalculatorTest extends Specification  {
    def "should return correct values as a mod calculator."()   {
        given:
            ModCalculator modCalculator = new ModCalculator()
        when:
            int returnValue = modCalculator.mod(value, mod)
        then:
            returnValue == expectedValue
        where:
            value   | mod   | expectedValue
            5       | 2     | 1
            6       | 3     | 0
            10      | 4     | 2
            20      | 5     | 0
    }
}
