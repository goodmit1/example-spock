package example.spock.parameterized

import spock.lang.Specification
import spock.lang.Unroll

/**
 * You can use your own Iterator to create parameterized values.
 */
class IteratorTest extends Specification  {
    @Unroll("#scenario.value % #scenario.mod = #scenario.expectedValue")
    def "should return correct values as a mod calculator."()   {
        given:
        ModCalculator modCalculator = new ModCalculator()
        when:
        int returnValue = modCalculator.mod(scenario.value, scenario.mod)
        then:
        returnValue == scenario.expectedValue
        where:
        scenario << new ScenarioIterator()
    }
}
