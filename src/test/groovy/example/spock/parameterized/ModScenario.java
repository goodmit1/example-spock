package example.spock.parameterized;

public class ModScenario {
    private int value;
    private int mod;
    private int expectedValue;

    public ModScenario(int value, int mod, int expectedValue)   {
        this.value = value;
        this.mod = mod;
        this.expectedValue = expectedValue;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getMod() {
        return mod;
    }

    public void setMod(int mod) {
        this.mod = mod;
    }

    public int getExpectedValue() {
        return expectedValue;
    }

    public void setExpectedValue(int expectedValue) {
        this.expectedValue = expectedValue;
    }
}
