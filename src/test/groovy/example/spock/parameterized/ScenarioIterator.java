package example.spock.parameterized;

import java.util.Iterator;

public class ScenarioIterator implements Iterator<ModScenario> {
    private int[] values = new int[] { 5,8,7,3,6 };
    private int[] mods = new int[] { 3, 4, 5, 6, 7 };
    private int[] expectedValues = new int[] { 2, 0, 2, 3, 6 };
    private int index = 0;

    @Override
    public boolean hasNext() {
        return index < values.length;
    }

    @Override
    public ModScenario next() {
        ModScenario modScenario = new ModScenario(values[index], mods[index], expectedValues[index]);
        index++;
        return modScenario;
    }
}
