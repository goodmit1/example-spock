package example.spock.parameterized

import spock.lang.Specification
import spock.lang.Unroll

/**
 * This test is similar to IteratorTest. The difference is that
 * you create test parameters with a table containing a value list.
 */
class ListOfParametersTest extends Specification  {
    @Unroll("#description")
    def "should return correct values as a mod calculator."()   {
        given:
        ModCalculator modCalculator = new ModCalculator()
        when:
        int returnValue = modCalculator.mod(scenario[0], scenario[1])
        then:
        returnValue == scenario[2]
        where:
        scenario | description
        [10, 5, 0] | "10 % 5 = 0"
        [8, 5, 3] | "8 % 5 = 3"
        [6, 4, 2] | "6 % 4 = 2"
    }
}
