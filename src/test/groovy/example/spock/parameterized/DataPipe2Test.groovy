package example.spock.parameterized

import spock.lang.Specification

class DataPipe2Test extends Specification  {
    def "Valid images are PNG and JPG files only"()    {
        given: "an image extension checker"
        ImageNameValidator validator = new ImageNameValidator()

        expect: "that only valid filenames are accepted"
        validator.isValidImageExtension(pictureFile) == validPicture

        where: "sample image names are"
        pictureFile  << ["scenery.jpg","house.jpeg", "car.png" ,"sky.tiff" ,"dance_bunny.gif" ]
        validPicture << [ true, false, true, false, false]
    }
}
