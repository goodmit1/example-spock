package example.spock.parameterized

import spock.lang.Specification
import spock.lang.Unroll

/**
 * 하나의 테스트 케이스에 대해서 조건 값들을 달리하여 각 조건값별로 test case가 각각 수행되며 값을 검증한다.
 */
class DataPipeTest extends Specification  {
    @Unroll("#first * #second should be negative")
    def "Multiplying is always a negative number"()  {
        expect:
            first * second < 0
        where:
            first << (10 .. 20)     // first 변수는 10부터 20까지의 값을 순차적으로 갖는다.
            second << (-1 .. -11)   // second 변수는 -1부터 -11까지 값을 순차적으로 갖는다.
    }

    @Unroll("#first * #second should be negative")
    def "Multiplying positive value and -1 is always a negative number"()  {
        expect:
        first * second < 0
        where:
        first << (10 .. 20)
        second = -1
    }
}
