package example.spock.parameterized

import spock.lang.Narrative
import spock.lang.Specification
import spock.lang.Title
import spock.lang.Unroll

/**
 * Unroll 어노테이션을 이용해서 파라미터 조건값을 test case 명에 넣을 수 있다.
 */
class UsingUnrollTest extends Specification  {
    @Unroll("#value % #mod = #expectedValue")
    def "should return correct values as a mod calculator."()   {
        given: "Using modCalculator"
        ModCalculator modCalculator = new ModCalculator()
        when: "mod operation for #value % #mod"
        int returnValue = modCalculator.mod(value, mod)
        then: "result should be #expectedValue"
        returnValue == expectedValue
        where:
        value   | mod   |expectedValue | description
        5       | 2     |1             | "5%2 = 1"
        6       | 3     |0              | "6%3 = 0"
        10      | 4     |2              | "10%4 = 2"
        20      | 5     |0              | "20%5 = 0"
    }
}
