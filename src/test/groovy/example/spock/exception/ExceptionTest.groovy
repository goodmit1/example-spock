package example.spock.exception

import example.spock.mock.Product
import example.spock.mock.WarehouseInventory
import spock.lang.Specification

/**
 * Exeption 발생을 검증하는 테스트이다.
 */
class ExceptionTest extends Specification   {
    def "Exception should be thrown for unknown products"() {
        given: "a warehouse"
        WarehouseInventory inventory = new WarehouseInventory()

        when: "warehouse is queried for the wrong product"
        inventory.getProductInventoryCount("productThatDoesNotExist")

        then: "an exception should be thrown"
        thrown(IllegalArgumentException)        // verify the specific exception has been thrown.
    }

    def "Error message for unknown products - better"() {
        given: "a warehouse"
        WarehouseInventory inventory = new WarehouseInventory()

        when: "warehouse is queried for the wrong product"
        inventory.getProductInventoryCount("productThatDoesNotExist")

        then: "an exception should be thrown"
        IllegalArgumentException e = thrown()
        e.message == "productThatDoesNotExist does not exist"   // verify a specific message is contained.
    }

    def "Negative quantity is the same as 0"()  {
        given: "a warehouse and a product"
        WarehouseInventory inventory = new WarehouseInventory()
        Product product = new Product(name: "TV", price: 30000, weight: 50)

        when: "inventory count of a product is negative"
        inventory.setProductInventoryCount(product.name, -5)

        then: "inventory count is 0 for that product"
        notThrown(IllegalArgumentException)                     // verity no exception is thrown.
        inventory.getProductInventoryCount(product.name) == 0
    }
}
