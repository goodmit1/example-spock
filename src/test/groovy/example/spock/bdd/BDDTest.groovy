package example.spock.bdd

import spock.lang.Narrative
import spock.lang.Specification
import spock.lang.Unroll

import javax.swing.text.ChangedCharSetException

@Narrative('고객이 신규 카드를 발급받으면 최초로 임의의 비밀번호가 할당된다. 고객이 자신이 원하는 비밀번호로 변경할 수 있다')
class BDDTest extends Specification {
    def "성공적으로 비밀번호 변경하기"() {
        given: "고객이 신규카드를 발급받았다"
        Card card = new Card("HDCard", "1234")
        Customer customer = new Customer()
        customer.addCard(card)

        when: "고객이 신규카드를 이전과는 다른 비밀번호로 수정한다"
        PasswordManager passwordManager = new PasswordManager()
        passwordManager.changePassword(customer, "HDCard", "5678")

        then: "고객의 신규카드의 비밀번호가 성공적으로 변경된다"
        customer.getCard("HDCard").password == "5678"
    }
    def "예전과 동일한 비밀번호로는 변경할 수 없다"() {
        given: "고객이 신규카드를 발급받았다"
        Card card = new Card("HDCard", "1234")
        Customer customer = new Customer()
        customer.addCard(card)

        when: "고객이 이전과는 동일한 번호로 변경한다"
        PasswordManager passwordManager = new PasswordManager()
        passwordManager.changePassword(customer, "HDCard", "1234")

        then: "고객의 비밀번호 변경이 실패하고, 예전 비밀번호를 그대로 유지한다"
        thrown(ChangePasswordException)
        customer.getCard("HDCard").password == "1234"
    }

    @Unroll
    def "간단한 비밀번호로는 변경할 수 없다. 비밀번호 #password - valid:#valid"()   {
        given: "고객은 비밀번호를 변경할 카드를 가지고 있다"
        Card card = new Card("HDCard", "3847")
        Customer customer = new Customer()
        customer.addCard(card)

        when: "고객은 카드의 비밀번호를 변경한다"
        then: "valid하지 않은 비밀번호이다"
        where:
        password | valid
        "1234" | false
        "1111" | false
        "2222" | false
        "3333" | false
        "4444" | false
        "5555" | false
        "6666" | false
        "7777" | false
        "8888" | false
        "9999" | false
        "0000" | false
        "4638" | true
    }
}

class Card  {
    String cardName;
    String password;

    Card(String cardName, String password) {
        this.cardName = cardName
        this.password = password
    }

    void changePassword(String password) throws ChangePasswordException  {
        if(this.password == password) throw new ChangePasswordException("이전과 동일한 비밀번호입니댜")
        this.password = password
    }
}

class Customer  {
    Map<String, Card> cards = new HashMap<>()

    void addCard(Card card)  {
        cards.put(card.cardName, card)
    }

    Card getCard(String cardName)    {
        return cards.get(cardName)
    }
}

class PasswordManager   {
    void changePassword(Customer customer, String cardName, String password) throws ChangePasswordException   {
        Card card = customer.getCard(cardName)
        if(card == null) throw new ChangePasswordException("존재하지 않는 카드입니다. cardName:$cardName")
        card.changePassword(password)
    }

    boolean isValidPassword(String password)    {
        def invalidPasswords = [ "1234", "1111", "2222", "3333", "4444", "5555", "6666", "7777", "8888", "9999", "0000"]
        if(invalidPasswords.contains(password)) return false
        return true;
    }
}