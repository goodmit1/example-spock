package example.spock.bdd

class ChangePasswordException extends Exception {
    ChangePasswordException() {
        super()
    }

    ChangePasswordException(String message) {
        super(message)
    }
}
