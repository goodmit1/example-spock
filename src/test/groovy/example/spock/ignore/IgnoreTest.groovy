package example.spock.ignore

import spock.lang.Ignore
import spock.lang.IgnoreIf
import spock.lang.Requires
import spock.lang.Specification

class IgnoreTest extends Specification {
    @Ignore
    def "ignored method is not tested"()    {
        expect:
        println "this is not tested"
    }

    @IgnoreIf({ new MyEnv().online == false })
    def "this test is ignored in offline environment"()    {
        expect:
        println "executed a test in online environment"
    }

    @Requires({
        MyEnv env = new MyEnv()
        env.online == false && env.os == "windows"
    })
    def "this test is executed when offline and os env"() {
        expect:
        println "executed a test in offline environment and os env"
    }
}

class MyEnv {
    boolean online = false
    String os = "windows"
}