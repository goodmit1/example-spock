package example.spock.mock

import spock.lang.Specification

/**
 *
 */
class ArgumentMatchTest extends Specification  {
    def "If warehouse has both products everything is fine"() {
        given:
            Product tv = new Product(name : "tv", price:5000, weight:18)
            Product camera = new Product(name: "camera", price:8000, weight:20)
        and:
            WarehouseInventory inventory = Stub(WarehouseInventory)
            inventory.isEmpty() >> false
            inventory.getProductInventoryCount(_) >> 1      // <-- return 1 for any parameter value.
            Basket basket  = new Basket(inventory)

        when:
            basket.addProduct(tv)
            basket.addProduct(camera)
        then:
            basket.canShipCompletely()
    }

    def "Inventory is always checked in the last possible moment"() {
        given:
            Product tv = new Product(name : "tv", price:5000, weight:18)
            Product camera = new Product(name: "camera", price:8000, weight:20)
        and:
            WarehouseInventory inventory = Stub(WarehouseInventory)
            inventory.isEmpty() >> false
            inventory.getProductInventoryCount(_) >>> 1 >> 0    // <-- sequence of values. First, return 1 and then 0.
            // inventory.getProductInventoryCount(_) >>> [ 1, 0 ]    // or you can use an array to sequence values.

        Basket basket  = new Basket(inventory)
        when:
            basket.addProduct(tv)
        then:
            basket.canShipCompletely()
        when:
            basket.addProduct(camera)
        then:
            ! basket.canShipCompletely()
    }

    def "closure example"() {
        given:
            Product tv = new Product(name : "tv", price:5000, weight:18)
            Product camera = new Product(name: "camera", price:8000, weight:20)
        and:
            WarehouseInventory inventory = Stub(WarehouseInventory)
            inventory.isEmpty() >> false
            inventory.getProductInventoryCount(_) >> {
                String productName -> return productName == "tv" ? 1 : 0
            }
            Basket basket  = new Basket(inventory)

        when:
        basket.addProduct(tv)
        then:
        basket.canShipCompletely()
        when:
        basket.addProduct(camera)
        then:
        ! basket.canShipCompletely()
    }
}
