package example.spock.mock

import spock.lang.Specification

/**
 * If you want to stub a mock method, it has a different way from the way of Stub.
 * In this example, A class is a mocked class and expect 2 times of calls for 'method1'.
 * '>>> [left, right]' part says that Spock should return stubbed values in the order.
 *
 */
class StubAMockMethodTest extends Specification {
    def "sum of values should be correct"()   {
        given:
        AA a = Mock(AA)
        BB b = new BB(a)

        when:
        def result = b.method1() + b.method1()

        then:
        result == expected
        2 * a.method1() >>> [left, right]       // stub a mock method with the order of left and right values.

        where:
        left | right | expected
        1    | 2     | 3
        4    | 5     | 9
    }
}

class AA {
    private int count = 0

    int method1()    {
        return count++
    }
}

class BB {
    def a

    BB(AA a) { this.a = a }

    int method1()    {
        return a.method1()
    }
}
