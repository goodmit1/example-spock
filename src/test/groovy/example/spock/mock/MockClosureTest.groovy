package example.spock.mock

import spock.lang.Specification

/**
 * 메소드 파라미터에 대해서 검증 로직을 closure 형태로 정의할 수 있다.
 *
 */
class MockClosureTest extends Specification {
    def "inventory should be checked"()  {
        given:
        Product tv = new Product(name: "tv", price: 1000, weight: 1000)
        and:
        WarehouseInventory inventory = Mock(WarehouseInventory)
        Basket basket = new Basket(inventory)
        basket.addProduct(tv)
        when:
        basket.canShipCompletely()
        then:
        // product 카테고리가 전자제품이어야 한다.
        1 * inventory.getProductInventoryCount({ productName -> productCategory(productName) == "electronic" } )
    }

    private String productCategory(String productName)  {
        if(productName == 'tv') return 'electronic'
        else return 'other'
    }
}
