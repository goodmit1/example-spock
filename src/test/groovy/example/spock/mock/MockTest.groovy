package example.spock.mock

import spock.lang.Specification

class MockTest extends  Specification {
    def "inventory should be checked"()  {
        given:
            Product tv = new Product(name: "tv", price: 1000, weight: 1000)
            Product tv2 = new Product(name: "tv", price: 1000, weight: 1000)
        and:
            WarehouseInventory inventory = Mock(WarehouseInventory)
            Basket basket = new Basket(inventory)
            basket.addProduct(tv)
            basket.addProduct(tv2)
        when:
            basket.canShipCompletely()
        then:
            2 * inventory.getProductInventoryCount(_) >> 1
            1 * inventory.isEmpty()
    }
    def "empty inventory should be checked before the stock of product"()  {
        given:
        Product tv = new Product(name: "tv", price: 1000, weight: 1000)
        and:
        WarehouseInventory inventory = Mock(WarehouseInventory)
        Basket basket = new Basket(inventory)
        basket.addProduct(tv)
        when:
        basket.canShipCompletely()
        // isEmpty() method is called at first and then getProductInventoryCount() is called.
        // If you change the sequence of the following codes the validation will fail.
        then:
        1 * inventory.isEmpty()
        then:
        1 * inventory.getProductInventoryCount(_)
    }

    def "If warehouse has the product on stock everything is fine"() {
        given: "a basket and a TV"
        WarehouseInventory inventory = Stub(WarehouseInventory)
        Basket basket = new Basket(inventory)

        and:"a warehouse with enough stock"
        inventory.getProductInventoryCount("bravia") >> 1
        inventory.isEmpty() >> false

        when: "user checks out the tv"
        Product tv = new Product(name:"bravia",price:1200,weight:18)
        basket.addProduct tv

        then: "order can be shipped right away"
        basket.canShipCompletely()
    }
}
