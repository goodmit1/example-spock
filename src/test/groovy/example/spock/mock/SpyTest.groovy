package example.spock.mock

import spock.lang.Specification

class SpyTest extends Specification  {
    def "Should return the number of files deleted"()   {
        given:
        def fileManager = Spy(FileManager)
        1 * fileManager.findFiles("directory/") >> { return ["file1", "file2", "file3", "file4"] }
        fileManager.deleteFile(_) >> { println "deleted file."}

        when:
        def count = fileManager.removeFiles("directory/")

        then:
        count == 4
    }

    def "FooService object should have true value for the executed field"()  {
        given:
        def fooService = Spy(FooService)
        fooService.doSomething("Sally") >> "A Spy can modify implementation"
        FooController sut = new FooController()
        sut.fooService = fooService

        when:
        def s = sut.doSomething()

        then:
        s == "A Spy can modify implementation"
        fooService.isExecuted() == true
    }
}
