package example.spock.mock

import spock.lang.Specification

/**
 * This is an example of Stub.
 * You need to tell Spock what to do when any of the stubbed methods are called.
 *
 */
class StubTest extends Specification  {
    def "If warehouse is empty nothing can be shipped"()    {
        given:
            Product tv = new Product(name : "tv", price:5000, weight:18)

        and:
            WarehouseInventory inventory = Stub(WarehouseInventory)     // create a Stub object
            inventory.isEmpty() >> true                                 // define return value of the stub object.

// or You can use the below style.
//            WarehouseInventory inventory = Stub(WarehouseInventory) {
//                isEmpty() >> true
//            }
            Basket basket  = new Basket(inventory)

        when:
            basket.addProduct(tv)
        then:
            ! basket.canShipCompletely()
    }

    def "If warehouse has the product on stock everything is fine"()    {
        given:
            Product tv = new Product(name : "tv", price:5000, weight:18)

        and:
            WarehouseInventory inventory = Stub(WarehouseInventory)
            inventory.isEmpty() >> false
            inventory.getProductInventoryCount("tv") >> 10  // define return value for a specific case.
            Basket basket  = new Basket(inventory)

        when:
            basket.addProduct(tv)

        then:
            basket.canShipCompletely()
    }
}
